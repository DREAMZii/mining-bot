package de.dreamzii.telegrambot.language;

import java.util.Locale;
import java.util.ResourceBundle;

public class LanguageManager {
    public static String getMessage(Locale locale, String key) {
        ResourceBundle bundle = ResourceBundle.getBundle("telegrambot", locale);
        return bundle.getString(key) + " ";
    }

    public static String getMessage(Locale locale, String key, Object... parameters) {
        ResourceBundle bundle = ResourceBundle.getBundle("telegrambot", locale);
        String result = bundle.getString(key) + " ";

        for (int i = 0; i < parameters.length; i++) {
            result = result.replace("{" + i +  "}", String.valueOf(parameters[i]));
        }

        return result;
    }
}
