package de.dreamzii.telegrambot.entity.user;

import java.util.List;
import java.util.Locale;

public interface UserDAO {
    UserDTO findByUserId(int userId);

    List<UserDTO> findInvestingUsers();

    boolean exists(String ip);

    long getLastWithdrawal(int userId);

    void updateLastWithdrawal(int userId);

    String getWalletId(int userId);

    double getBalance(int userId);

    void addWalletId(int userId, String walletId);

    void addBalance(int userId, double amount);

    void setLocale(int userId, Locale locale);

    boolean hasWalletId(int userId);

    boolean isAlreadyRegistered(String walletId);

    int getUserId(String walletId);

    Locale getLocale(int userId);

    boolean exists(int userId);

    UserDTO getOrSave(UserDTO dto);
}
