package de.dreamzii.telegrambot.entity.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "SELECT * FROM `user` WHERE user_id = ?1", nativeQuery = true)
    User findByUserId(int userId);

    @Query(value = "SELECT * FROM `user` WHERE balance > 0", nativeQuery = true)
    List<User> findInvestingUsers();

    @Query(value = "SELECT EXISTS(SELECT 1 FROM `user` WHERE ip = ?1 LIMIT 1)", nativeQuery = true)
    int exists(String ip);

    @Query(value = "SELECT last_withdrawal FROM `user` WHERE user_id = ?1", nativeQuery = true)
    long getLastWithdrawal(int userId);

    @Query(value = "SELECT wallet_id FROM `user` WHERE user_id = ?1", nativeQuery = true)
    String getWalletId(int userId);

    @Query(value = "SELECT balance FROM `user` WHERE user_id = ?1", nativeQuery = true)
    double getBalance(int userId);

    @Query(value = "SELECT wallet_id FROM `user` WHERE wallet_id = ?1", nativeQuery = true)
    String isRegistered(String walletId);

    @Query(value = "SELECT user_id FROM `user` WHERE wallet_id = ?1", nativeQuery = true)
    int getUserId(String walletId);

    @Query(value = "SELECT locale FROM `user` WHERE user_id = ?1", nativeQuery = true)
    String getLocale(int userId);
}
