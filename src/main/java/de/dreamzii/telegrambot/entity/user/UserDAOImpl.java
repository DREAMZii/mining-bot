package de.dreamzii.telegrambot.entity.user;

import de.dreamzii.telegrambot.dao.BaseDAO;
import de.dreamzii.telegrambot.dao.DAOManager;
import de.dreamzii.telegrambot.dao.Injectable;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Service
public class UserDAOImpl extends BaseDAO implements UserDAO, Injectable {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EntityManagerFactory factory;

    @Override
    public void inject(DAOManager daoManager) {

    }

    @Override
    public UserDTO findByUserId(int userId) {
        return fromEntity(userRepository.findByUserId(userId));
    }

    @Override
    public List<UserDTO> findInvestingUsers() {
        return fromEntityList(userRepository.findInvestingUsers());
    }

    @Override
    public boolean exists(String ip) {
        return userRepository.exists(ip) == 1;
    }

    @Override
    public long getLastWithdrawal(int userId) {
        return userRepository.getLastWithdrawal(userId);
    }

    @Override
    public void updateLastWithdrawal(int userId) {
        SessionFactory sessionFactory = factory.unwrap(SessionFactory.class);
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = sessionFactory.openSession();
        }
        Query query = session.createSQLQuery("UPDATE `user` SET `last_withdrawal` = :timestamp WHERE `user_id` = :user_id");
        query.setParameter("user_id", userId);
        query.setParameter("timestamp", System.currentTimeMillis());
        query.executeUpdate();
        session.close();
    }

    @Override
    public String getWalletId(int userId) {
        return userRepository.getWalletId(userId);
    }

    @Override
    public double getBalance(int userId) {
        return userRepository.getBalance(userId);
    }

    @Async
    @Override
    public void addWalletId(int userId, String walletId) {
        SessionFactory sessionFactory = factory.unwrap(SessionFactory.class);
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = sessionFactory.openSession();
        }
        Query query = session.createSQLQuery("UPDATE `user` SET `wallet_id` = :wallet_id WHERE `user_id` = :user_id");
        query.setParameter("user_id", userId);
        query.setParameter("wallet_id", walletId);
        query.executeUpdate();
        session.close();
    }

    @Async
    @Override
    public void addBalance(int userId, double amount) {
        SessionFactory sessionFactory = factory.unwrap(SessionFactory.class);
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = sessionFactory.openSession();
        }
        Query query = session.createSQLQuery("UPDATE `user` SET `balance` = :balance WHERE `user_id` = :user_id");
        query.setParameter("user_id", userId);
        query.setParameter("balance", getBalance(userId) + amount);
        query.executeUpdate();
        session.close();
    }

    @Async
    @Override
    public void setLocale(int userId, Locale locale) {
        SessionFactory sessionFactory = factory.unwrap(SessionFactory.class);
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = sessionFactory.openSession();
        }
        Query query = session.createSQLQuery("UPDATE `user` SET `locale` = :locale WHERE `user_id` = :user_id");
        query.setParameter("user_id", userId);
        query.setParameter("locale", locale.getLanguage());
        query.executeUpdate();
        session.close();
    }

    @Override
    public boolean hasWalletId(int userId) {
        return getWalletId(userId) != null && !getWalletId(userId).isEmpty();
    }

    @Override
    public boolean isAlreadyRegistered(String walletId) {
        return userRepository.isRegistered(walletId) != null;
    }

    @Override
    public int getUserId(String walletId) {
        return userRepository.getUserId(walletId);
    }

    @Override
    public Locale getLocale(int userId) {
        return Locale.forLanguageTag(userRepository.getLocale(userId));
    }

    @Override
    public boolean exists(int userId) {
        return userRepository.findByUserId(userId) != null;
    }

    @Override
    public UserDTO getOrSave(UserDTO dto) {
        int id = dto.getUserId();
        if (exists(id)) {
            return findByUserId(id);
        }
        return fromEntity(userRepository.save(new User(id, dto.getUserName(), dto.getWalletId(), dto.getLocale(), dto.getBalance(), dto.getIp(), dto.getLastWithdrawal())));
    }

    private UserDTO fromEntity(User user) {
        return new UserDTO.Builder(user.getUserId(), user.getUserName(), user.getBalance(), user.getIp(), user.getLastWithdrawal()).withLocale(user.getLocale()).withWalletId(user.getWalletId()).build();
    }

    private List<UserDTO> fromEntityList(List<User> users) {
        List<UserDTO> dtoList = new ArrayList<>();
        users.forEach(user ->
            dtoList.add(fromEntity(user))
        );
        return dtoList;
    }
}
