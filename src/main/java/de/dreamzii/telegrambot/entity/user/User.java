package de.dreamzii.telegrambot.entity.user;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Getter
public class User implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private int userId;

    @Column
    private String userName;

    @Column
    private String walletId;

    @Column
    private String locale;

    @Column(nullable = false)
    private double balance;

    @Column(nullable = false)
    private String ip;

    @Column
    private long lastWithdrawal;

    protected User() {
        // Required
    }

    public User(int userId, String userName, String walletId, String locale, double balance, String ip, long lastWithdrawal) {
        this.userId = userId;
        this.userName = userName;
        this.userId = userId;
        this.walletId = walletId;
        this.locale = locale;
        this.balance = balance;
        this.ip = ip;
        this.lastWithdrawal = lastWithdrawal;
    }
}
