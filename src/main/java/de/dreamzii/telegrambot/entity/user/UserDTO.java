package de.dreamzii.telegrambot.entity.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserDTO {
    private int userId;
    private String userName;
    private String walletId;
    private String locale;
    private double balance;
    private String ip;
    private long lastWithdrawal;

    public static class Builder {
        private String userName;
        private int userId;
        private String walletId;
        private String locale;
        private double balance;
        private String ip;
        private long lastWithdrawal;

        public Builder(int userId, String userName, double balance, String ip, long lastWithdrawal) {
            this.userName = userName;
            this.userId = userId;
            this.walletId = "";
            this.balance = balance;
            this.ip = ip;
            this.lastWithdrawal = lastWithdrawal;
        }

        public Builder withWalletId(String walletId) {
            this.walletId = walletId;
            return this;
        }

        public Builder withLocale(String languageTag) {
            this.locale = languageTag;
            return this;
        }

        public UserDTO build() {
            return new UserDTO(userId, userName, walletId, locale, balance, ip, lastWithdrawal);
        }
    }
}
