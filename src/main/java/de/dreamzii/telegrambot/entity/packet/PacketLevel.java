package de.dreamzii.telegrambot.entity.packet;

import de.dreamzii.telegrambot.entity.referral.ReferralLevel;
import lombok.Getter;

import java.text.DecimalFormat;

public enum PacketLevel {
    PACKET_1(1, ReferralLevel.LEVEL_1, 0.04),
    PACKET_2(2, ReferralLevel.LEVEL_2, 0.05),
    PACKET_3(3, ReferralLevel.LEVEL_3, 0.1),
    PACKET_4(4, ReferralLevel.LEVEL_4, 1);

    @Getter
    private int id;
    @Getter
    private ReferralLevel level;
    @Getter
    private double price;

    PacketLevel(int id, ReferralLevel level, double price) {
        this.id = id;
        this.level = level;
        this.price = price;
    }

    public String getReferralLevelName() {
        return "Level " + level.getId();
    }

    public String getFormattedPrice() {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        return decimalFormat.format(this.price).replaceAll(",", ".");
    }

    public static PacketLevel getById(int id) {
        for(PacketLevel packet : values()) {
            if (packet.id == id) {
                return packet;
            }
        }
        return null;
    }

    public static PacketLevel getByPrice(double amount) {
        for(PacketLevel packet : values()) {
            if (packet.price == amount) {
                return packet;
            }
        }
        return null;
    }
}
