package de.dreamzii.telegrambot.entity.packet;

import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.dao.BaseDAO;
import de.dreamzii.telegrambot.dao.DAOManager;
import de.dreamzii.telegrambot.dao.Injectable;
import de.dreamzii.telegrambot.entity.log.LogDAO;
import de.dreamzii.telegrambot.entity.log.LogDTO;
import de.dreamzii.telegrambot.entity.log.LogOperation;
import de.dreamzii.telegrambot.entity.referral.ReferralDAO;
import de.dreamzii.telegrambot.entity.referral.ReferralDTO;
import de.dreamzii.telegrambot.entity.referral.ReferralLevel;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import org.aspectj.weaver.ast.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.groupadministration.GetChat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class PacketDAOImpl extends BaseDAO implements PacketDAO, Injectable {
    @Autowired
    private PacketRepository packetRepository;
    @Autowired
    private LogDAO log;
    @Autowired
    private ReferralDAO dao;
    @Autowired
    private UserDAO userDao;

    @Override
    public void inject(DAOManager daoManager) {

    }

    @Override
    public List<PacketDTO> findByUserId(int userId) {
        return fromEntityList(packetRepository.findByUserId(userId));
    }

    @Override
    public PacketLevel getPacketLevel(int userId) {
        List<PacketDTO> dtos = findByUserId(userId);
        AtomicReference<PacketLevel> level = new AtomicReference<>(PacketLevel.PACKET_1);
        dtos.forEach(dto -> {
           if (dto.getPackageNumber() > level.get().getId()) {
               level.set(PacketLevel.getById(dto.getPackageNumber()));
           }
        });

        return level.get();
    }

    @Override
    public boolean exists(String txid) {
        return packetRepository.exists(txid) == 1;
    }

    // Packet bought
    @Async
    @Override
    public void create(PacketDTO dto) {
        if (exists(dto.getTxid())) {
            return;
        }

        // Create Log for buying something
        double amount = PacketLevel.getById(dto.getPackageNumber()).getPrice();

        List<ReferralDTO> dtos = dao.findLayerReferrer(dto.getUserId());
        dtos.forEach(referral -> {
            double ref = amount * (double) ReferralLevel.getById(referral.getLevel()).getPercentage() / 100;
            // Add balance
            userDao.addBalance(referral.getUserId(), ref);

            log.create(new LogDTO.Builder(referral.getUserId(), LogOperation.REF_BONUS, ref, System.currentTimeMillis()).build());
        });

        // Save buy and add balance
        userDao.addBalance(dto.getUserId(), amount);
        packetRepository.save(new Packet(dto.getUserId(), dto.getPackageNumber(), dto.getTxid(), dto.getTimestamp()));
        log.create(new LogDTO.Builder(dto.getUserId(), LogOperation.BUY, amount, System.currentTimeMillis()).build());
    }

    private PacketDTO fromEntity(Packet packet) {
        return new PacketDTO.Builder(packet.getUserId(), packet.getPackageNumber(), packet.getTxid(), packet.getTimestamp()).build();
    }

    private List<PacketDTO> fromEntityList(List<Packet> packets) {
        List<PacketDTO> dtoList = new ArrayList<>();
        packets.forEach(packet ->
            dtoList.add(fromEntity(packet))
        );
        return dtoList;
    }
}
