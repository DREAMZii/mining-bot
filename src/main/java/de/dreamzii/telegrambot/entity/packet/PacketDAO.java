package de.dreamzii.telegrambot.entity.packet;

import de.dreamzii.telegrambot.entity.referral.Referral;

import java.util.List;

public interface PacketDAO {
    List<PacketDTO> findByUserId(int userId);

    PacketLevel getPacketLevel(int userId);

    boolean exists(String txid);

    void create(PacketDTO dto);
}
