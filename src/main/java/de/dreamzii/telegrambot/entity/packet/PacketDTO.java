package de.dreamzii.telegrambot.entity.packet;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PacketDTO {
    private int userId;
    private int packageNumber;
    private String txid;
    private long timestamp;

    public static class Builder {
        private int userId;
        private int packageNumber;
        private String txid;
        private long timestamp;

        public Builder(int userId, int packageNumber, String txid, long timestamp) {
            this.userId = userId;
            this.packageNumber = packageNumber;
            this.txid = txid;
            this.timestamp = timestamp;
        }

        public PacketDTO build() {
            return new PacketDTO(userId, packageNumber, txid, timestamp);
        }
    }
}
