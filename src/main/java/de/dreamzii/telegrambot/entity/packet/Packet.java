package de.dreamzii.telegrambot.entity.packet;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Getter
public class Packet implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private int userId;

    @Column(nullable = false)
    private int packageNumber;

    @Column(nullable = false)
    private String txid;

    @Column(nullable = false)
    private long timestamp;

    protected Packet() {
        // Required
    }

    public Packet(int userId, int packageNumber, String txid, long timestamp) {
        this.userId = userId;
        this.packageNumber = packageNumber;
        this.txid = txid;
        this.timestamp = timestamp;
    }
}
