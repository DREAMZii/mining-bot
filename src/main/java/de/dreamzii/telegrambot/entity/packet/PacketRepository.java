package de.dreamzii.telegrambot.entity.packet;

import de.dreamzii.telegrambot.entity.referral.Referral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PacketRepository extends JpaRepository<Packet, Integer> {
    @Query(value = "SELECT * FROM `packet` WHERE user_id = ?1", nativeQuery = true)
    List<Packet> findByUserId(int userId);

    @Query(value = "SELECT EXISTS(SELECT 1 FROM `packet` WHERE txid = ?1 LIMIT 1)", nativeQuery = true)
    int exists(String txid);
}
