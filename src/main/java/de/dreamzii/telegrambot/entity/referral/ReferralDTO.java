package de.dreamzii.telegrambot.entity.referral;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ReferralDTO {
    private int userId;
    private int referralId;
    private int level;

    public static class Builder {
        private int userId;
        private int referralId;
        private int level;

        public Builder(int userId, int referralId, int level) {
            this.userId = userId;
            this.referralId = referralId;
            this.level = level;
        }

        public ReferralDTO build() {
            return new ReferralDTO(userId, referralId, level);
        }
    }
}
