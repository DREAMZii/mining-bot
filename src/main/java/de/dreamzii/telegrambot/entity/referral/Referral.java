package de.dreamzii.telegrambot.entity.referral;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Getter
public class Referral implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    // user that referred
    @Column(nullable = false)
    private int userId;

    // user being referred
    @Column(nullable = false)
    private int referralId;

    @Column(nullable = false)
    private int level;

    protected Referral() {
        // Required
    }

    public Referral(int userId, int referralId, int level) {
        this.userId = userId;
        this.referralId = referralId;
        this.level = level;
    }
}
