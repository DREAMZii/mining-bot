package de.dreamzii.telegrambot.entity.referral;

import de.dreamzii.telegrambot.dao.BaseDAO;
import de.dreamzii.telegrambot.dao.DAOManager;
import de.dreamzii.telegrambot.dao.Injectable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReferralDAOImpl extends BaseDAO implements ReferralDAO, Injectable {
    @Autowired
    private ReferralRepository referralRepository;

    @Override
    public void inject(DAOManager daoManager) {

    }

    @Override
    public List<ReferralDTO> findByUserId(int userId) {
        return fromEntityList(referralRepository.findByUserId(userId));
    }

    @Override
    public ReferralDTO findReferrer(int referralId) {
        Referral r = referralRepository.findReferrer(referralId);
        return r == null ? null : fromEntity(referralRepository.findReferrer(referralId));
    }

    @Override
    public List<ReferralDTO> findLayerReferrer(int referralId) {
        List<Referral> r = referralRepository.findLayerReferrer(referralId);
        return r == null || r.isEmpty() ? new ArrayList<>() : fromEntityList(referralRepository.findLayerReferrer(referralId));
    }

    @Override
    public int findAmountOfLevel(int userId, int level) {
        return referralRepository.findAmountOfLevel(userId, level);
    }

    @Override
    public boolean exists(int referralId) {
        return referralRepository.exists(referralId) == 1;
    }

    @Async
    @Override
    public void create(ReferralDTO dto) {
        referralRepository.save(new Referral(dto.getUserId(), dto.getReferralId(), dto.getLevel()));
    }

    private ReferralDTO fromEntity(Referral referral) {
        return new ReferralDTO.Builder(referral.getUserId(), referral.getReferralId(), referral.getLevel()).build();
    }

    private List<ReferralDTO> fromEntityList(List<Referral> referrals) {
        List<ReferralDTO> dtoList = new ArrayList<>();
        referrals.forEach(referral ->
            dtoList.add(new ReferralDTO.Builder(referral.getUserId(), referral.getReferralId(), referral.getLevel()).build())
        );
        return dtoList;
    }
}
