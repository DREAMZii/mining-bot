package de.dreamzii.telegrambot.entity.referral;

import java.util.List;

public interface ReferralDAO {
    List<ReferralDTO> findByUserId(int userId);

    ReferralDTO findReferrer(int referralId);

    List<ReferralDTO> findLayerReferrer(int referralId);

    int findAmountOfLevel(int userId, int level);

    boolean exists(int referralId);

    void create(ReferralDTO dto);
}
