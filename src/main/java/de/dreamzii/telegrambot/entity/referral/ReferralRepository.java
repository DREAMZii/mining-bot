package de.dreamzii.telegrambot.entity.referral;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReferralRepository extends JpaRepository<Referral, Integer> {
    @Query(value = "SELECT * FROM `referral` WHERE user_id = ?1", nativeQuery = true)
    List<Referral> findByUserId(int userId);

    @Query(value = "SELECT * FROM `referral` WHERE referral_id = ?1 AND level = 1", nativeQuery = true)
    Referral findReferrer(int referralId);

    @Query(value = "SELECT * FROM `referral` WHERE referral_id = ?1", nativeQuery = true)
    List<Referral> findLayerReferrer(int referralId);

    @Query(value = "SELECT COUNT(*) FROM `referral` WHERE user_id = ?1 AND level = ?2", nativeQuery = true)
    int findAmountOfLevel(int userId, int level);

    @Query(value = "SELECT EXISTS(SELECT 1 FROM `referral` WHERE referral_id = ?1 LIMIT 1)", nativeQuery = true)
    int exists(int referralId);
}
