package de.dreamzii.telegrambot.entity.referral;

import com.google.common.util.concurrent.AtomicDouble;
import de.dreamzii.telegrambot.entity.packet.PacketDAO;
import de.dreamzii.telegrambot.entity.packet.PacketDTO;
import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.entity.user.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ReferralManager {
    private UserDAO userDao;
    private ReferralDAO referralDao;
    private PacketDAO packetDao;

    @Autowired
    private ReferralManager(UserDAO userDao, ReferralDAO referralDao, PacketDAO packetDao) {
        this.userDao = userDao;
        this.referralDao = referralDao;
        this.packetDao = packetDao;
    }

    // Refers a user
    public void refer(int userId, int referralId) {
        // Dont refer yourself
        if (userId == referralId) {
            return;
        }

        // Every user can be referred once!
        // User has to exist
        if (userDao.exists(referralId) &&
                !referralDao.exists(referralId)) {
            List<ReferralDTO> dtosToSave = getLevelAbove(userId, referralId, 1, new ArrayList<>(Arrays.asList(new ReferralDTO.Builder(userId, referralId, 1).build())));
            dtosToSave.forEach(referralDao::create);
        }
    }

    // Recurse iteration to get levels above the referral
    private List<ReferralDTO> getLevelAbove(int userId, int referralId, int layer, List<ReferralDTO> toSave) {
        if (layer > PacketLevel.values().length) {
            System.out.println("Layer over " + PacketLevel.values().length);
            return toSave;
        }

        ReferralDTO dto = referralDao.findReferrer(userId);
        if (dto == null) {
            System.out.println("No more layers above " + layer);
            return toSave;
        }
        PacketLevel availableLevel = packetDao.getPacketLevel(dto.getUserId());
        System.out.println("Available Level for " + dto.getUserId() + " is " + availableLevel.getId());

        if (availableLevel.getId() > layer) {
            System.out.println("Added to save!");
            toSave.add(new ReferralDTO.Builder(dto.getUserId(), referralId, layer + 1).build());
        }

        return getLevelAbove(dto.getUserId(), referralId, layer + 1, toSave);
    }

    public int getAmountOfReferralsOnLevel(int userId, int level) {
        return referralDao.findAmountOfLevel(userId, level);
    }

    public String getReflink(int userId) {
        return "https://telegram.me/test_timdo_bot?start=" + userId;
    }
}
