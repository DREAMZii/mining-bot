package de.dreamzii.telegrambot.entity.referral;

import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import lombok.Getter;

public enum ReferralLevel {
    LEVEL_1(1, 8),
    LEVEL_2(2, 1),
    LEVEL_3(3, 1),
    LEVEL_4(4, 5);

    @Getter
    private int id;
    @Getter
    private int percentage;

    ReferralLevel(int id, int percentage) {
        this.id = id;
        this.percentage = percentage;
    }

    public static ReferralLevel getById(int id) {
        for(ReferralLevel level : values()) {
            if (level.id == id) {
                return level;
            }
        }
        return null;
    }
}
