package de.dreamzii.telegrambot.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LogDTO {
    private int userId;
    private String operation;
    private double amount;
    private long timestamp;

    public static class Builder {
        private int userId;
        private String operation;
        private double amount;
        private long timestamp;

        public Builder(int userId, String operation, double amount, long timestamp) {
            this.userId = userId;
            this.operation = operation;
            this.amount = amount;
            this.timestamp = timestamp;
        }

        public LogDTO build() {
            return new LogDTO(userId, operation, amount, timestamp);
        }
    }
}
