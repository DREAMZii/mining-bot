package de.dreamzii.telegrambot.entity.log;

import de.dreamzii.telegrambot.entity.packet.Packet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LogRepository extends JpaRepository<Log, Integer> {
    @Query(value = "SELECT * FROM (" +
            "SELECT * FROM log WHERE user_id = ?1 ORDER BY id DESC LIMIT 10" +
            ") sub ORDER BY id DESC", nativeQuery = true)
    List<Log> findByUserId(int userId);
}
