package de.dreamzii.telegrambot.entity.log;

import de.dreamzii.telegrambot.dao.BaseDAO;
import de.dreamzii.telegrambot.dao.DAOManager;
import de.dreamzii.telegrambot.dao.Injectable;
import de.dreamzii.telegrambot.entity.packet.Packet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class LogDAOImpl extends BaseDAO implements LogDAO, Injectable {
    @Autowired
    private LogRepository logRepository;

    @Override
    public void inject(DAOManager daoManager) {

    }

    @Override
    public List<LogDTO> findByUserId(int userId) {
        return fromEntityList(logRepository.findByUserId(userId));
    }

    @Async
    @Override
    public void create(LogDTO dto) {
        logRepository.save(new Log(dto.getUserId(), dto.getOperation(), dto.getAmount(), dto.getTimestamp()));
    }

    private LogDTO fromEntity(Log log) {
        return new LogDTO.Builder(log.getUserId(), log.getOperation(), log.getAmount(), log.getTimestamp()).build();
    }

    private List<LogDTO> fromEntityList(List<Log> logs) {
        List<LogDTO> dtoList = new ArrayList<>();
        logs.forEach(log ->
            dtoList.add(fromEntity(log))
        );
        return dtoList;
    }
}
