package de.dreamzii.telegrambot.entity.log;

import java.util.List;

public interface LogDAO {
    List<LogDTO> findByUserId(int userId);

    void create(LogDTO dto);
}
