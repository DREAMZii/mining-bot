package de.dreamzii.telegrambot.entity.log;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Getter
public class Log implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private int userId;

    @Column(nullable = false)
    private String operation;

    @Column
    private double amount = 0D;

    @Column(nullable = false)
    private long timestamp;

    protected Log() {
        // Required
    }

    public Log(int userId, String operation, double amount, long timestamp) {
        this.userId = userId;
        this.operation = operation;
        this.amount = amount;
        this.timestamp = timestamp;
    }
}
