package de.dreamzii.telegrambot.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
public class ApiGeneralConfig {
    /**
     * Bean for retrieving datasource.
     * @return Initialized DataSource.
     */
    @Bean
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource apiDataSource(){
        return DataSourceBuilder.create().build();
    }

    /**
     * Bean for retrieving JdbcTemplate.
     * @param dataSource Instance of DataSource.
     * @return Instance of JdbcTemplate.
     */
    @Bean
    public JdbcTemplate apiJdbcTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
}

