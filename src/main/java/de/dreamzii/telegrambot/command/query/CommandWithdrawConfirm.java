package de.dreamzii.telegrambot.command.query;

import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommandQuery;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.language.LanguageManager;
import de.dreamzii.telegrambot.rest.BlockIOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.text.DecimalFormat;
import java.util.Locale;

@Component
public class CommandWithdrawConfirm implements BotCommandQuery {
    private UserDAO dao;
    private BlockIOService service;

    @Autowired
    private CommandWithdrawConfirm(UserDAO dao, BlockIOService service) {
        this.dao = dao;
        this.service = service;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("withdraw_confirm", this);
    }

    @Override
    public void execute(int userId, long chatId, String data) {
        Locale locale = dao.getLocale(userId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);

        double amount = dao.getBalance(userId) / 2;
        boolean success = service.withdraw(userId, amount);

        if (success) {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.withdraw.success", amount));
        } else {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.no.balance"));
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
