package de.dreamzii.telegrambot.command.query;

import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommandQuery;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.language.LanguageManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.text.DecimalFormat;
import java.util.Locale;

@Component
public class CommandPacketInfo implements BotCommandQuery {
    private UserDAO dao;

    @Autowired
    private CommandPacketInfo(UserDAO dao) {
        this.dao = dao;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("packet_1", this, "packet_2", "packet_3", "packet_4");
    }

    @Override
    public void execute(int userId, long chatId, String data) {
        Locale locale = dao.getLocale(userId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        PacketLevel level;
        switch (data) {
            case "packet_1":
                level = PacketLevel.PACKET_1;
                sendMessage.setText(getPacketInfo(locale, level));
                break;
            case "packet_2":
                level = PacketLevel.PACKET_2;
                sendMessage.setText(getPacketInfo(locale, level));
                break;
            case "packet_3":
                level = PacketLevel.PACKET_3;
                sendMessage.setText(getPacketInfo(locale, level));
                break;
            case "packet_4":
                level = PacketLevel.PACKET_4;
                sendMessage.setText(getPacketInfo(locale, level));
                break;
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private String getPacketInfo(Locale locale, PacketLevel level) {
        return LanguageManager.getMessage(locale,
                "bot.querycommand.packet",
                String.valueOf(level.getId()),
                level.getFormattedPrice(),
                level.getReferralLevelName(),
                getProfit(level));
    }

    private String getProfit(PacketLevel level) {
        DecimalFormat format = new DecimalFormat("0.000000");
        return format.format(level.getPrice() * 0.6);
    }
}
