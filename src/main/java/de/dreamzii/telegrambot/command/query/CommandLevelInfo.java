package de.dreamzii.telegrambot.command.query;

import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommandQuery;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.referral.ReferralDAO;
import de.dreamzii.telegrambot.entity.referral.ReferralDTO;
import de.dreamzii.telegrambot.entity.referral.ReferralLevel;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.entity.user.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.List;

@Component
public class CommandLevelInfo implements BotCommandQuery {
    private UserDAO dao;
    private ReferralDAO referralDao;

    @Autowired
    private CommandLevelInfo(UserDAO dao, ReferralDAO referralDao) {
        this.dao = dao;
        this.referralDao = referralDao;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("level_1", this, "level_2", "level_3", "level_4");
    }

    @Override
    public void execute(int userId, long chatId, String data) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        ReferralLevel level;
        switch (data) {
            case "level_1":
                level = ReferralLevel.LEVEL_1;
                sendMessage.setText(getReferrals(userId, level.getId()));
                break;
            case "level_2":
                level = ReferralLevel.LEVEL_2;
                sendMessage.setText(getReferrals(userId, level.getId()));
                break;
            case "level_3":
                level = ReferralLevel.LEVEL_3;
                sendMessage.setText(getReferrals(userId, level.getId()));
                break;
            case "level_4":
                level = ReferralLevel.LEVEL_4;
                sendMessage.setText(getReferrals(userId, level.getId()));
                break;
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private String getReferrals(int userId, int level) {
        StringBuilder builder = new StringBuilder();
        List<ReferralDTO> dtos = referralDao.findByUserId(userId);
        dtos.forEach(dto -> {
            if (dto.getLevel() == level) {
                UserDTO user = dao.findByUserId(dto.getReferralId());
                if (user.getUserName() != null && !user.getUserName().isEmpty()) {
                    builder.append("@" + user.getUserName() + ", ");
                } else {
                    builder.append(user.getUserId() + ", ");
                }
            }
        });

        if (builder.length() > 2) {
            return builder.deleteCharAt(builder.length() - 2).toString();
        } else {
            return "No Refs in this Level!";
        }
    }
}
