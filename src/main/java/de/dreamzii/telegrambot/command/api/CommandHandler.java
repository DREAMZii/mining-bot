package de.dreamzii.telegrambot.command.api;

import org.telegram.telegrambots.api.objects.Message;

import java.util.HashMap;
import java.util.Map;

public class CommandHandler {
    private static final Map<String, BotCommand> commandMap = new HashMap<>();
    private static final Map<String, BotCommandQuery> queryCommandMap = new HashMap<>();

    public static void register(String name, BotCommand command, String... aliases) {
        commandMap.put(name, command);
        System.out.println("Registered Command: " + name);

        // Register aliases
        if (aliases.length > 0) {
            String[] strippedAliases = new String[aliases.length - 1];
            for (int i = 1; i <= aliases.length - 1; i++) {
                strippedAliases[i - 1] = aliases[i];
            }
            register(aliases[0], command, strippedAliases);
        }
    }

    public static void register(String name, BotCommandQuery command, String... aliases) {
        queryCommandMap.put(name, command);
        System.out.println("Registered Querycommand: " + name);

        // Register aliases
        if (aliases.length > 0) {
            String[] strippedAliases = new String[aliases.length - 1];
            for (int i = 1; i <= aliases.length - 1; i++) {
                strippedAliases[i - 1] = aliases[i];
            }
            register(aliases[0], command, strippedAliases);
        }
    }

    public static boolean execute(String commandName, Message message) {
        if (!commandMap.containsKey(commandName)) {
            return false;
        }

        BotCommand command = commandMap.get(commandName);
        command.execute(message);
        return true;
    }

    public static boolean execute(String commandName, int userId, long chatId, String data) {
        if (!queryCommandMap.containsKey(commandName)) {
            return false;
        }

        BotCommandQuery command = queryCommandMap.get(commandName);
        command.execute(userId, chatId, data);
        return true;
    }
}
