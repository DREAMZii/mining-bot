package de.dreamzii.telegrambot.command.api;

import org.telegram.telegrambots.api.objects.Message;

public interface BotCommand {
    void register();

    void execute(Message message);
}
