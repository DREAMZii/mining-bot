package de.dreamzii.telegrambot.command.api;

public interface BotCommandQuery {
    void register();

    void execute(int userId, long chatId, String data);
}
