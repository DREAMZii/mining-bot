package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Keyboard;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.language.LanguageManager;
import de.dreamzii.telegrambot.rest.BlockIOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.Locale;

@Component
public class CommandWithdraw implements BotCommand {
    private UserDAO dao;
    private BlockIOService service;

    @Autowired
    private CommandWithdraw(UserDAO dao, BlockIOService service) {
        this.dao = dao;
        this.service = service;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Auszahlen", this, "Withdraw");
    }

    @Override
    public void execute(Message message) {
        int userId = message.getFrom().getId();
        Locale locale = dao.getLocale(userId);

        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.enableHtml(true);
        sendMessage.setChatId(message.getChatId());

        if (dao.hasWalletId(userId)) {
            int diffDays = getWithdrawalDifference(userId);
            if (diffDays < 7) {
                sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.time", (7 - diffDays)));
            } else {
                double withdrawal = dao.getBalance(userId) / 2;

                sendMessage.setReplyMarkup(Keyboard.getWithdrawalKeyboard(locale, withdrawal));
                sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.withdraw"));
            }
        } else {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.no.address"));
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private int getWithdrawalDifference(int userId) {
        long diff = System.currentTimeMillis() - dao.getLastWithdrawal(userId);
        return (int) (diff / (24 * 60 * 60 * 1000));
    }
}
