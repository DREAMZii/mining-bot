package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Emoji;
import de.dreamzii.telegrambot.bot.Keyboard;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.packet.PacketDAO;
import de.dreamzii.telegrambot.entity.packet.PacketDTO;
import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import de.dreamzii.telegrambot.language.LanguageManager;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

@Component
public class CommandBalance implements BotCommand {
    private UserDAO dao;
    private PacketDAO packetDao;

    private double PERCENT_PER_HOUR = 0.25;

    @Autowired
    private CommandBalance(UserDAO dao, PacketDAO packetDao) {
        this.dao = dao;
        this.packetDao = packetDao;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Meine", this, "My");
    }

    @Override
    public void execute(Message message) {
        int userId = message.getFrom().getId();
        Locale locale = dao.getLocale(userId);
        String balance = getFormattedDouble(dao.getBalance(userId));

        SendMessage sendMessage = new SendMessage();
        sendMessage.setReplyMarkup(Keyboard.getInvestKeyboard(balance, locale));
        sendMessage.enableMarkdown(true);
        sendMessage.enableHtml(true);
        sendMessage.setChatId(message.getChatId());

        sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.balance", balance,
                getTotalPackets(userId),
                getProfit(userId)));

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private String getTotalPackets(int userId) {
        List<PacketDTO> dtos = packetDao.findByUserId(userId);
        double spent = 0;
        for (PacketDTO dto : dtos) {
            spent += PacketLevel.getById(dto.getPackageNumber()).getPrice();
        }
        return getFormattedDouble(spent);
    }

    private String getProfit(int userId) {
        return getFormattedDouble(dao.getBalance(userId) - Double.valueOf(getTotalPackets(userId)));
    }

    private String getFormattedDouble(double toFormat) {
        DecimalFormat format = new DecimalFormat("0.000000");
        return format.format(toFormat).replaceAll(",", ".");
    }
}
