package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Keyboard;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.language.LanguageManager;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.Locale;

@Component
public class CommandLanguage implements BotCommand {
    private UserDAO dao;

    @Autowired
    private CommandLanguage(UserDAO dao) {
        this.dao = dao;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Deutsch", this, "English");
    }

    @Override
    public void execute(Message message) {
        Locale locale = message.getText().contains("Deutsch") ? Locale.GERMAN : Locale.ENGLISH;
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyMarkup(Keyboard.getInvestKeyboard("0.000000", locale));
        sendMessage.setChatId(message.getChatId());
        sendMessage.setText(LanguageManager.getMessage(locale, "bot.setup.chosen.language") + message.getText());

        int userId = message.getFrom().getId();
        dao.setLocale(userId, locale);

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
