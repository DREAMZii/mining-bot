package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Keyboard;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.log.LogDAO;
import de.dreamzii.telegrambot.entity.log.LogDTO;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.language.LanguageManager;
import org.apache.commons.codec.language.bm.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component
public class CommandLog implements BotCommand {
    private UserDAO dao;
    private LogDAO logDao;

    @Autowired
    private CommandLog(UserDAO dao, LogDAO logDao) {
        this.dao = dao;
        this.logDao = logDao;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Log", this);
    }

    @Override
    public void execute(Message message) {
        int userId = message.getFrom().getId();
        Locale locale = dao.getLocale(userId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableHtml(true);
        sendMessage.setChatId(message.getChatId());

        List<LogDTO> dtos = logDao.findByUserId(userId);
        if (dtos.isEmpty()) {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.no.log"));
        } else {
            StringBuilder builder = new StringBuilder(LanguageManager.getMessage(locale, "bot.command.log") + "\n");

            dtos.forEach(dto ->
                builder.append(LanguageManager.getMessage(locale, "bot.command.log.template",
                        dto.getOperation(),
                        new DecimalFormat("0.000000").format(dto.getAmount()),
                        new Date(dto.getTimestamp())))
                        .append("\n\n")
            );

            sendMessage.setText(builder.toString());
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
