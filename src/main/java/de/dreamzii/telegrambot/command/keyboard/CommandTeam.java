package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Keyboard;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.packet.PacketDAO;
import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import de.dreamzii.telegrambot.entity.referral.ReferralDAO;
import de.dreamzii.telegrambot.entity.referral.ReferralManager;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.entity.user.UserDTO;
import de.dreamzii.telegrambot.language.LanguageManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.security.Key;
import java.util.Locale;

@Component
public class CommandTeam implements BotCommand {
    private UserDAO dao;
    private PacketDAO packetDao;
    private ReferralDAO referralDao;
    private ReferralManager manager;

    @Autowired
    private CommandTeam(UserDAO dao, PacketDAO packetDao, ReferralDAO referralDao, ReferralManager manager) {
        this.dao = dao;
        this.packetDao = packetDao;
        this.referralDao = referralDao;
        this.manager = manager;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Unser", this, "Our");
    }

    @Override
    public void execute(Message message) {
        int userId = message.getFrom().getId();
        Locale locale = dao.getLocale(userId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.enableHtml(true);
        sendMessage.disableWebPagePreview();
        sendMessage.setChatId(message.getChatId());

        PacketLevel level = packetDao.getPacketLevel(userId);

        sendMessage.setReplyMarkup(Keyboard.getTeamKeyboard(userId, manager));
        sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.reflink",
                manager.getReflink(userId),
                level.getId(),
                // FIXME
                0.000000,
                0.000000));

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}

