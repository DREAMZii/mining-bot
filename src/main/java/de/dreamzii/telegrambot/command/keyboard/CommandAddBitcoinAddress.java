package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.language.LanguageManager;
import de.dreamzii.telegrambot.rest.BlockIOService;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.Locale;
import java.util.regex.Pattern;

@Component
public class CommandAddBitcoinAddress implements BotCommand {
    private UserDAO dao;
    private BlockIOService service;
    private String REGEX = "^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$";

    @Autowired
    private CommandAddBitcoinAddress(UserDAO dao, BlockIOService service) {
        this.dao = dao;
        this.service = service;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Hinzufügen:", this, "Add:");
    }

    @Override
    public void execute(Message message) {
        String text = message.getText();
        int userId = message.getFrom().getId();
        Locale locale = dao.getLocale(userId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId());

        // Validation
        String walletId = text.split(": ")[1].trim();
        // Check if correct REGEX
        if (!Pattern.matches(REGEX, walletId)) {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.wrong.format"));
        } else if(walletId.equals(service.getPacketAddress())) {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.same.address"));
        } else if(dao.isAlreadyRegistered(walletId)) {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.already.added"));
        } else {
            dao.addWalletId(userId, walletId);
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.bitcoinaddress.add"));
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
