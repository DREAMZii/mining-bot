package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Emoji;
import de.dreamzii.telegrambot.bot.Keyboard;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.language.LanguageManager;
import de.dreamzii.telegrambot.rest.BlockIOService;
import de.dreamzii.telegrambot.rest.BlockchainService;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
public class CommandPacket implements BotCommand {
    private UserDAO dao;
    private BlockIOService service;

    @Autowired
    private CommandPacket(UserDAO dao, BlockIOService service) {
        this.dao = dao;
        this.service = service;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Paket", this, "Buy");
    }

    @Override
    public void execute(Message message) {
        int userId = message.getFrom().getId();
        Locale locale = dao.getLocale(userId);
        String address = service.getPacketAddress();

        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.enableHtml(true);
        sendMessage.setChatId(message.getChatId());

        if (dao.hasWalletId(userId)) {
            sendMessage.setReplyMarkup(Keyboard.getPacketKeyboard());
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.packet", address));
        } else {
            sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.error.no.address"));
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
