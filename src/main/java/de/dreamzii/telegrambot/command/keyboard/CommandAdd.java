package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Emoji;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.language.LanguageManager;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.Locale;

@Component
public class CommandAdd implements BotCommand {
    private UserDAO dao;

    @Autowired
    private CommandAdd(UserDAO dao) {
        this.dao = dao;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("Hinzufügen", this, "Add");
    }

    @Override
    public void execute(Message message) {
        int userId = message.getFrom().getId();
        Locale locale = dao.getLocale(userId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId());
        sendMessage.setText(LanguageManager.getMessage(locale, "bot.command.add") + Emoji.PLUS.getUnicode());

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
