package de.dreamzii.telegrambot.command.keyboard;

import de.dreamzii.telegrambot.bot.Keyboard;
import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.command.api.BotCommand;
import de.dreamzii.telegrambot.command.api.CommandHandler;
import de.dreamzii.telegrambot.entity.referral.ReferralDAO;
import de.dreamzii.telegrambot.entity.referral.ReferralManager;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.entity.user.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

@Component
public class CommandStart implements BotCommand {
    private UserDAO dao;
    private ReferralManager manager;

    @Autowired
    private CommandStart(UserDAO dao, ReferralManager manager) {
        this.dao = dao;
        this.manager = manager;
        register();
    }

    @Override
    public void register() {
        CommandHandler.register("/start", this);
    }

    @Override
    public void execute(Message message) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyMarkup(Keyboard.LANGUAGE_KEYBOARD);
        sendMessage.setChatId(message.getChatId());
        sendMessage.setText("Please select your language!");

        // Save to database
        int userId = message.getFrom().getId();
        String userName = message.getFrom().getUserName();
        double amount = dao.exists(userId) ? dao.getBalance(userId) : 0;
        UserDTO dto = new UserDTO.Builder(userId, userName, amount, "", System.currentTimeMillis()).build();
        dao.getOrSave(dto);

        String[] possibleUserId = message.getText().split(" ");
        if (possibleUserId.length > 1) {
            int referrerId = 0;
            try {
                referrerId = Integer.parseInt(possibleUserId[1]);
            } catch(NumberFormatException e) {
                // Silent catch to prevent
            }

            manager.refer(referrerId, userId);
        }

        try {
            TestBotHandler.getInstance().sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
