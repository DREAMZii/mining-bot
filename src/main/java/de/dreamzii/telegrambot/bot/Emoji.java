package de.dreamzii.telegrambot.bot;

public enum Emoji {
    MONEY_BAG("Moneybag", "\uD83D\uDCB0"),
    BANKNOTE("Banknote", "\uD83D\uDCB5"),
    BRIEFCASE("Briefcase", "\uD83D\uDCBC"),
    CLOCKWISE_RIGHTWARDS("Clockwise Rightwards", "\uD83D\uDD01"),
    HOURGLASS("Hourglass", "\u23F3"),
    PLUS("Heavy Plus Sign", "\u2795"),
    MINUS("Heavy Minus Sign", "\u2796"),
    BUSTS_IN_SILHOUETTE("Busts in Silhouette", "\uD83D\uDC65"),
    LIGHT_BULB("Light Bulb", "\uD83D\uDCA1"),
    GERMAN_FLAG("German Flag", "\uD83C\uDDE9\uD83C\uDDEA"),
    GB_FLAG("Great Britain Flag", "\uD83C\uDDEC\uD83C\uDDE7"),
    FIRE("Fire", "\uD83D\uDD25")
    ;

    private String name;
    private String unicode;

    Emoji(String name, String unicode) {
        this.name = name;
        this.unicode = unicode;
    }

    public String getName() {
        return name;
    }

    public String getUnicode() {
        return unicode;
    }

}
