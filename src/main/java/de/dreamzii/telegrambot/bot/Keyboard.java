package de.dreamzii.telegrambot.bot;

import de.dreamzii.telegrambot.entity.log.LogDTO;
import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import de.dreamzii.telegrambot.entity.referral.ReferralLevel;
import de.dreamzii.telegrambot.entity.referral.ReferralManager;
import de.dreamzii.telegrambot.language.LanguageManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Keyboard {
    public static ReplyKeyboardMarkup LANGUAGE_KEYBOARD = getLanguageKeyboard();

    private static ReplyKeyboardMarkup getLanguageKeyboard() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);

        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add("Deutsch " + Emoji.GERMAN_FLAG.getUnicode());
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add("English " + Emoji.GB_FLAG.getUnicode());
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        replyKeyboardMarkup.setKeyboard(keyboard);

        return replyKeyboardMarkup;
    }

    public static ReplyKeyboardMarkup getInvestKeyboard(String balance, Locale locale) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);

        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(LanguageManager.getMessage(locale, "keyboard.invest.balance", balance) + Emoji.MONEY_BAG.getUnicode());
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(LanguageManager.getMessage(locale, "keyboard.invest.log", balance) + Emoji.HOURGLASS.getUnicode());
        KeyboardRow keyboardThirdRow = new KeyboardRow();
        keyboardThirdRow.add(LanguageManager.getMessage(locale, "keyboard.invest.invest") + Emoji.BANKNOTE.getUnicode());
        keyboardThirdRow.add(LanguageManager.getMessage(locale, "keyboard.invest.withdraw") + Emoji.BRIEFCASE.getUnicode());
        KeyboardRow keyboardFifthRow = new KeyboardRow();
        keyboardFifthRow.add(LanguageManager.getMessage(locale, "keyboard.invest.team") + Emoji.BUSTS_IN_SILHOUETTE.getUnicode());
        keyboardFifthRow.add(LanguageManager.getMessage(locale, "keyboard.invest.help") + Emoji.LIGHT_BULB.getUnicode());
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        keyboard.add(keyboardThirdRow);
        keyboard.add(keyboardFifthRow);
        replyKeyboardMarkup.setKeyboard(keyboard);

        return replyKeyboardMarkup;
    }

    public static InlineKeyboardMarkup getPacketKeyboard() {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        for (PacketLevel packet : PacketLevel.values()) {
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(
                    new InlineKeyboardButton()
                            .setText(packet.getId() + ": " + packet.getFormattedPrice())
                            .setCallbackData("packet_" + packet.getId())
            );
            // Set the keyboard to the markup
            rowsInline.add(rowInline);
        }
        // Add it to the message
        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }

    public static InlineKeyboardMarkup getTeamKeyboard(int userId, ReferralManager manager) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        for (ReferralLevel level : ReferralLevel.values()) {
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(
                    new InlineKeyboardButton()
                            .setText("Level " + level.getId() + ": " + manager.getAmountOfReferralsOnLevel(userId, level.getId()))
                            .setCallbackData("level_" + level.getId())
            );
            // Set the keyboard to the markup
            rowsInline.add(rowInline);
        }
        // Add it to the message
        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }

    public static InlineKeyboardMarkup getWithdrawalKeyboard(Locale locale, double amount) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        rowInline.add(
                new InlineKeyboardButton()
                        .setText(LanguageManager.getMessage(locale, "bot.command.withdraw.confirm", new DecimalFormat("0.000000").format(amount).replaceAll(",", ".")))
                        .setCallbackData("withdraw_confirm")
        );
        // Set the keyboard to the markup
        rowsInline.add(rowInline);
        // Add it to the message
        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }
}
