package de.dreamzii.telegrambot.bot;

import de.dreamzii.telegrambot.command.api.CommandHandler;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.lang.reflect.Field;

public class TestBotHandler extends TelegramLongPollingBot {
    private static TestBotHandler instance;
    private static String REGEX = "^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$";

    public static TestBotHandler getInstance() {
        if (instance == null) {
            instance = new TestBotHandler();
        }
        return instance;
    }

    private final static String USERNAME = "test_timdo_bot";
    private final static String AUTH_TOKEN = "348186703:AAHiARnwSHo3G9F0iRQG1jR3vMGdp6SoBOg";

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasCallbackQuery()) {
            final CallbackQuery query = update.getCallbackQuery();
            final String data = query.getData();
            handleMessage(data, query.getFrom().getId(), query.getMessage().getChatId());
        }

        if (update.hasMessage()) {
            final Message message = update.getMessage();
            // Do it dirty, edit message with reflection
            if (message.getText().matches(REGEX)) {
                try {
                    Field textField = message.getClass().getDeclaredField("text");
                    textField.setAccessible(true);
                    textField.set(message, "Add: " + message.getText());
                    textField.setAccessible(false);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    // Nothing to handle here, just continue
                }
            }

            String possibleCommand = message.getText().split(" ")[0];
            handleMessage(possibleCommand, message);
        }
    }

    private void handleMessage(String possibleCommand, Message message) {
        boolean success = CommandHandler.execute(possibleCommand, message);
        if (!success) {
            sendCommandNotFound(message.getChatId());
        }
    }

    private void handleMessage(String possibleCommand, int userId, long chatId) {
        boolean success = CommandHandler.execute(possibleCommand, userId, chatId, possibleCommand);
        if (!success) {
            sendCommandNotFound(chatId);
        }
    }

    private void sendCommandNotFound(long chatId) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        // FIXME i18n
        sendMessage.setText("Kein gültiger Command!");
        try {
            this.sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return USERNAME;
    }

    @Override
    public String getBotToken() {
        return AUTH_TOKEN;
    }
}
