package de.dreamzii.telegrambot.rest;

import de.dreamzii.telegrambot.entity.packet.PacketDTO;
import de.dreamzii.telegrambot.entity.packet.PacketLevel;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class BlockIOService {
    private UserDAO dao;

    @Autowired
    private BlockIOService(UserDAO dao) {
        this.dao = dao;
    }

    private String BASE_URL = "https://block.io/api/v2/";
    private String API_KEY = "/?api_key=b3a1-7d23-00ea-0a35";

    private RestTemplate restTemplate = new RestTemplate();

    public String getInvestmentAddress() {
        return getAddress(0);
    }

    public String getPacketAddress() {
        return getAddress(1);
    }

    private String getAddress(int index) {
        String requestUrl = BASE_URL + "get_my_addresses_without_balances" + API_KEY;

        String json = restTemplate.getForObject(requestUrl, String.class);
        JSONObject jsonObject = new JSONObject(json);
        String address = jsonObject.getJSONObject("data")
                .getJSONArray("addresses")
                .getJSONObject(index)
                .getString("address");

        return address;
    }

    public List<PacketDTO> getLastPackets() {
        String requestUrl = BASE_URL + "get_transactions" + API_KEY + "&type=received&addresses=" + getPacketAddress();

        List<PacketDTO> packets = new ArrayList<>();

        String json = restTemplate.getForObject(requestUrl, String.class);
        JSONObject jsonObject = new JSONObject(json);
        JSONArray txs = jsonObject.getJSONObject("data").getJSONArray("txs");
        for (int i = 0; i < txs.length(); i++) {
            JSONObject tx = txs.getJSONObject(i);

            String walletId = tx.getJSONArray("senders").getString(0);
            int userId = dao.getUserId(walletId);
            String txid = tx.getString("txid");
            double amount = tx.getJSONArray("amounts_received").getJSONObject(0).getDouble("amount");
            PacketLevel level = PacketLevel.getByPrice(amount);

            if (level == null) {
                continue;
            }
            PacketDTO dto = new PacketDTO.Builder(userId, level.getId(), txid, System.currentTimeMillis()).build();
            packets.add(dto);
        }

        return packets;
    }

    public boolean withdraw(int userId, double amount) {
        String requestUrl = BASE_URL +
                "withdraw" +
                API_KEY +
                "&amounts=" +
                new DecimalFormat("0.000000").format(amount).replaceAll(",", ".") +
                "&to_addresses=" + dao.getWalletId(userId);

        try {
            restTemplate.getForObject(requestUrl, String.class);
        } catch (HttpClientErrorException e) {
            return false;
        }
        dao.updateLastWithdrawal(userId);
        dao.addBalance(userId, (amount * -1));
        return true;
    }
}
