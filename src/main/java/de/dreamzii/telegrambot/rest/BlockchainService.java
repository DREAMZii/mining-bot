package de.dreamzii.telegrambot.rest;

import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

public class BlockchainService {
    private static RestTemplate restTemplate = new RestTemplate();

    private static String URL = "https://blockchain.info/de/ticker";
    private static String EURO_CODE = "EUR";
    private static String DOLLAR_CODE = "USD";

    public static double getCurrentEuroValue() {
        String json = restTemplate.getForObject(URL, String.class);
        JSONObject jsonObject = new JSONObject(json);
        double currentValue = jsonObject.getJSONObject(EURO_CODE).getDouble("last");

        return currentValue;
    }

    public static double getCurrentUSDValue() {
        String json = restTemplate.getForObject(URL, String.class);
        JSONObject jsonObject = new JSONObject(json);
        double currentValue = jsonObject.getJSONObject(DOLLAR_CODE).getDouble("last");

        return currentValue;
    }
}
