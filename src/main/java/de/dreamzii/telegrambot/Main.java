package de.dreamzii.telegrambot;

import de.dreamzii.telegrambot.bot.TestBotHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

@SpringBootApplication
@ComponentScan("de.dreamzii.telegrambot")
@EnableJpaRepositories("de.dreamzii.telegrambot")
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@EnableScheduling
@EnableAsync
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);

        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        /*try {
            new Mail().send("tim.do1@web.de", "Test", "Test 123 üü k");
        } catch (IOException | EmailException e) {
            e.printStackTrace();
        }*/

        try {
            telegramBotsApi.registerBot(TestBotHandler.getInstance());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }
}
