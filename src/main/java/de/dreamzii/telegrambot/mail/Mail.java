package de.dreamzii.telegrambot.mail;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import java.io.IOException;

public class Mail {
    private final String mailserver = "smtp.gmail.com";
    private final String username = "dreamziixxx@gmail.com";
    private final String password = "kikololo776";
    private final String sender = "test_timdo_bot@irgendwas.com";
    private final String textCharset = "UTF-8";

    public String send(String receiver, String subject, String text) throws IOException, EmailException {
        MultiPartEmail email = new MultiPartEmail();
        email.setAuthenticator(new GMailAuthenticator(username, password));
        email.setSSL(true);
        email.setTLS(true);
        email.setHostName(mailserver);
        email.setFrom(sender);
        email.setCharset(textCharset);
        email.addTo(receiver);
        email.setSubject(subject);
        email.setMsg(text);
        return email.send();
    }
}
