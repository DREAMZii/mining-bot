package de.dreamzii.telegrambot.dao;

public interface Injectable {
    void inject(DAOManager daoManager);
}
