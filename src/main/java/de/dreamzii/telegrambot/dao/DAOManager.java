package de.dreamzii.telegrambot.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Component
public class DAOManager {
    /**
     * Holds a Set of Injectables. Automatically instanciated by Spring.
     */
    @Autowired
    private Set<Injectable> injectables = new HashSet<>();

    /**
     * Called after DAOManager got instanciated. Calls method inject() on all DAOs implementing interface Injectable.
     */
    @PostConstruct
    private void inject() {
        for (Injectable injectableItem : injectables) {
            injectableItem.inject(this);
        }
    }
}
