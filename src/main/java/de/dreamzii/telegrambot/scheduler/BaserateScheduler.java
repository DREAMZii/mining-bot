package de.dreamzii.telegrambot.scheduler;

import de.dreamzii.telegrambot.entity.log.LogDAO;
import de.dreamzii.telegrambot.entity.log.LogDTO;
import de.dreamzii.telegrambot.entity.log.LogOperation;
import de.dreamzii.telegrambot.entity.packet.PacketDAO;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.entity.user.UserDTO;
import de.dreamzii.telegrambot.rest.BlockIOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BaserateScheduler {
    private UserDAO userDAO;
    private LogDAO log;
    private BlockIOService service;

    private double PERCENT_PER_WEEK = 0.075;

    @Autowired
    private BaserateScheduler(UserDAO userDAO, LogDAO log, BlockIOService service) {
        this.userDAO = userDAO;
        this.log = log;
        this.service = service;
    }

    // Per hour (start an hour after start of bot)
    @Scheduled(cron = "0 0 0 * * SUN")
    public void updateBalance() {
        List<UserDTO> dtos = userDAO.findInvestingUsers();
        dtos.forEach(dto -> {
            double balance = dto.getBalance();
            double profit = balance * PERCENT_PER_WEEK;

            userDAO.addBalance(dto.getUserId(), profit);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        System.out.println("FINISHED BASE_RATE");
    }
}
