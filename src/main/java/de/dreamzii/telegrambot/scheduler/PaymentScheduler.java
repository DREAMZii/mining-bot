package de.dreamzii.telegrambot.scheduler;

import de.dreamzii.telegrambot.bot.TestBotHandler;
import de.dreamzii.telegrambot.entity.log.LogDAO;
import de.dreamzii.telegrambot.entity.log.LogDTO;
import de.dreamzii.telegrambot.entity.log.LogOperation;
import de.dreamzii.telegrambot.entity.packet.PacketDAO;
import de.dreamzii.telegrambot.entity.packet.PacketDTO;
import de.dreamzii.telegrambot.entity.user.UserDAO;
import de.dreamzii.telegrambot.entity.user.UserDTO;
import de.dreamzii.telegrambot.rest.BlockIOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PaymentScheduler {
    private PacketDAO packetDAO;
    private BlockIOService service;

    @Autowired
    private PaymentScheduler(PacketDAO packetDAO, BlockIOService service) {
        this.packetDAO = packetDAO;
        this.service = service;
    }

    // per 5 seconds
    @Scheduled(fixedRate = 5000)
    public void checkPackets() {
        List<PacketDTO> dtos = service.getLastPackets();
        dtos.forEach(dto ->
            packetDAO.create(dto)
        );
    }
}
